\documentclass{article}

\usepackage{graphicx}

\begin{document}


\section{Introduction}

Baseline dependent averaging (BDA) exploits the fact that shorter baselines have less variability in the visibilities and can
therefore be averaged further. There are two sources of variability: the geometric phase and the gains.
In nearly all cases there is larger variability in the geometric phases than in the gains.
Currently both are treated the same and evaluated at the same resolution.
The variability of the geometric phase depends on baseline length, the variability of the instrumental gains does not depend
on baseline length. However, if the gains include ionospheric effects, then their variability also depends on baseline length.

For efficiency reasons, the geometric phase and the gains are computed per station and then applied per baseline.
For BDA this introduces a problem because a station participates in multiple baselines which may have different lengths and
therefore different averaging factors.


\begin{figure}
    \includegraphics[width=0.5\textwidth]{intervals.pdf}
    \caption{BDA intervals}
    \label{fig:intervals}
\end{figure}


\begin{figure}
    \includegraphics[width=0.5\textwidth]{aliasing2.pdf}
    \caption{Spectrum of a factor 10 averaged sinusoid resampled (nearest neighbor) to factor 11 averaging}
    \label{fig:aliasing}
\end{figure}

Figure \ref{fig:intervals} shows the BDA intervals for different averaging factors and there midpoints.
Note that the midpoints for even averaging factors end up half way the midpoints of the unaveraged intervals.

If the exact mid points of the intervals are required than the phasors need to be evaluated at twice
the unaveraged rate.

Otherwise some form of interpolation is needed. The computationally cheapest form of interpolation is nearest neighbor
rounding. For odd vs even intervals this causes a systematic lead or lag by half the step size.
For other ratios this can cause aliasing, see Figure \ref{fig:aliasing}


\section{Ratio work per station and work per baseline}
The work per station is an sincos evaluation per time frequency sample.
The work per baseline is a complex multiplication of the two phasors, and then a complex multiplication per correlation product
(usually 4: XX, XY, YX, YY), so 5 complex multiplications per time frequency sample.
The number of baselines is (at most, if no selection is made) $N(N-1)/2$, where $N$ is the number of antennas. The ratio between the number of
baselines and number of antennas is $(N-1)/2$.

How big a fraction of the work can be saved by computing the per baseline work at the BDA resolution, depends on 1) the ratio
between the cost of a sincos evaluation vs a complex multiplication, 2) the BDA compression factor, and 3)
the ratio between number of baselines and number of stations.

This works out differently for different arrays, LOFAR-HBA (N=76), SKA-MID (N=197) and SKA-LOW (512).

For LOFAR this amounts to 188 complex multiplications (application of the phasor) per sincos evaluation (computation of the phasor).
For SKA-LOW this amounts to 1278 complex multiplications per sincos evaluation.



\section{Beam Evaluations}
Evaluation of the array factor involves the computation of a phasor, or sincos evaluation, per element.
For SKA-LOW the number of elements is 256. Beam evaluation is much more expensive then phasor computation.
This still disregards the evaluation of the element beam, which is also expensive.

The effect of the beam is not baseline dependent. Averaging a short baseline over a certain interval implies
that the beam is assumed to be constant over this interval. Longer baselines are averaged over shorter intervals
because the phasor varies more quickly. The beam however does not vary more, so for longer baselines it is not necessary to
evaluate the beam more often than for shorter baselines. The rate for the shortest baseline is also sufficient for the other baselines.


\section{Time and bandwidth smearing}
In DP3 time and bandwidth smearing are treated differently.
There is time smearing correction by upsampling, enabled by parset key "correcttimesmearing" (Predict.cc).
And there is frequency smearing correction,  by sinc evaluation, enabled by the (undocumented) parset key "correctfreqsmearing" (OnePredict.cc).

Upsampling does only partial correction, up to the level of the upsampled interval. Smearing within this interval is ignored.
Correction by the sinc function takes the full effect into account (but ignores higher resolution flags).
Smearing is baseline dependent, so a sinc correction requires a sine evaluation per baseline.

The sinc correction is for one dimension only. The two dimensional integral, over time and frequency, yields a gamma function,
which is even (a lot) more expensive to compute then a sine function.

However the correction changes only slowly over time and frequency, so the correction factor can be evaluated at a lower resolution.

\section{Conclusions}

\begin{itemize}

\item
Expanding to full resolution then predict and average is a straightforward and accurate approach,
as it mimics the actual signal path most closely. For the LOFAR case this is probably still the best approach.

\item
Extending this approach with IDGPredict is straightforward to implement. It is beneficial in cases where complex source models are used.
It does nearly the same thing as the current approach except that the beam is only evaluated at the aterm update intervals.

\item
For SKA-LOW it might be interesting to predict the BDAed visibilities directly, because the number of stations is
much larger then for LOFAR.

\item
Great care must be taken however to select the rate at which the phasors are computed and the interpolation scheme that will be used.
This to prevent systematic effects like aliasing.

\item
A lot of computational cost can be avoided by evaluating the beam at lower time frequency resolution.
This a probably okay in most cases except maybe when subtracting the brightest sources.

\item
The expand-average approach includes time and bandwidth smearing, up to the level of the unaveraged resolution.
Direct BDA predict would need to compute the smearing factor to correct for it. This is an expensive function,
but it can probably evaluated at lower resolution.

\end{itemize}


\end{document}



